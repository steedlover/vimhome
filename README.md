## Install steps ##

### 1) Cloning ###
```
#!bash

git clone https://steedlover@bitbucket.org/steedlover/vimhome.git -b full ~/vimhome
cp ~/vimhome/.vimrc ~/vimhome/.vim ~/ -R
```
### 2) Installing vundle ###
```
#!bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```
### 3) Installing all plugins ###
```
#!bash
vim +PluginInstall +qall
```
### 4) Install necessary tools to make YouCompleteMe (Ubuntu sample) ###
```
#!bash
sudo apt-get install build-essential cmake
sudo apt-get install python-dev python3-dev
```
### 5) Compile YouCompleteMe plugin ###
```
#!bash
cd ~/.vim/bundle/YouCompleteMe/
python3 ./install.py --all (--ts-completer for TypeScript support only)
```

### 6) VIM prettier installation
#### Install dependencies
```
#!bash
cd ~/.vim/bundle/vim-prettier
npm install
```
#### XML support
Xml is not completely supported by Prettier, so there is a function in the vimrc config file which does the job, but it uses external program Tidy.

Ubuntu way of the Tidy program installation:
```
sudo apt-get install tidy
```

### 7) Install ruby and compile Command-t plugin ###
```
#!bash
sudo apt-get install ruby-dev (Ubuntu requirements)
cd ~/.vim/bundle/command-t/
rake make
```
### 8) Installing jshint globally for javascript checking on saving ###
```
#!bash
npm install -g jshint
```

### 9) Installing necessary npm packets for a errors checking in the react-native JSX files ###
```
#!bash
npm install -g eslint
npm install -g babel-eslint
npm install -g eslint-plugin-react

```

### 10) Installing necessary TypeScript packages ###
```
#!bash
npm -g install typescript
cd ~/.vim/bundle/vimproc.vim && make
```

### 11) Installing packages to make the ELM plugin work ###
```
#!bash
npm install -g elm elm-test elm-oracle elm-format elm-language-server
```
If you see the error - 'compiled javascript file not found' during
the plugins installing, start this command in VIM:
> :call coc#util#install()

### 12) Installing icons for decoration of the lightline status bar

The best option is to install the patched font from the repository
```
#!bash
git clone https://github.com/ryanoasis/nerd-fonts ~/Downloads/nerd-fonts
cd ~/Downloads/nerd-fonts
./install.sh Hack
```

Also to make it work in Konsole (with Kubuntu), don't forget
to add a profile with new font- *Hack Nerd Font 10*
Good explanation is [here](https://askubuntu.com/a/93823)

Also For Debian/Ubuntu users some ofthe powerline icons  are accessable from repository
```
#!bash
sudo apt-get install fonts-powerline
```

### 13) TMUX config

I binded the prefix for Ctr+Space combination

No special config is needed, just copy the *.tmux.conf* file and the *.tmux* folder to your homepage folder

Also to make the battery indicator work copy this script:
```
#!bash
curl -OL https://raw.github.com/richo/battery/master/bin/battery
mv battery ~/.local/bin/battery
chmod +x ~/.local/bin/battery
```

### 14) CSS snippets

You can add CSS snippets for your omnicompletion window

For this grap the file and copy it into the snippets vim folder:
```
wget https://raw.githubusercontent.com/csexton/snipmate.vim/master/snippets/css.snippets -P ~/vim/snippets/
```
And uncomment the `set omnifunc=csscomplete#CompleteCSS` line in the ~/.vimrc file

Snippets could be displayed on <C-x><C-o> keys press
