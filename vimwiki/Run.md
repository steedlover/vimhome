:console:command:

= Run a console command from VIM without exiting =

You can run any command, just put the exclamation sign

== Examples ==

* Copy files
    :!cp ~/.vimrc ~/www/vimhome
