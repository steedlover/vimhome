:vimrc:config:

= Update .vimrc config without reopening vim itself =
----

:so(urce) {config_file_path}

== Example ==

:source ~/.vimrc
