:search:pattern:grep:vimgrep:regex:

= Search for a pattern in the project files =
----

:vimgrep /some\ pattern/ relative/path/for/searching/**/*

== Flags ==

* Without the 'g' flag each line is added only once.
    > With 'g' every match is added.

* Without the 'j' flag Vim jumps to the first match.
    > With 'j' only the quickfix list is updated.

=== Commands for controlling the quickfix window with results ===

* :copen " Open the quickfix window
* :ccl   " Close it
* :cw    " Open it if there are "errors", close it otherwise (some people prefer this)
* :cn    " Go to the next error in the window
* :cp    " Go to the previous error in the window
* :cnf   " Go to the first error in the next file

== Examples ==

:vimgrep /express\ vitals/ form/view/**/*.hbs

== For case sensetive search add \C into the pattern ==

:vimgrep /express\ vitals\C/ form/view/**/*.hbs

=== *\c* for case ignore (by default) ===

:vimgrep /express\ vitals\c/ form/view/**/*.hbs

=== To limit number of results add integer before command ===

Quick fix for the very first result

:1vimgrep /express\ vitals\c/ form/view/**/*.hbs

Or limit for 5 results

:5vimgrep /express\ vitals\c/ form/view/**/*.hbs
