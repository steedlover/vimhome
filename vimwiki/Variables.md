:var:variable:programming:

= Types of Vim variables =
----

To create a variable:
    :let g:var="some"
    
To remove variable:
    :unlet g:var
    
== Types of variables ==

Prefix shows the scope of the variable
    buffer-variable    b:    Local to the current buffer.
    window-variable    w:    Local to the current window.
    tabpage-variable   t:    Local to the current tab page.
    global-variable    g:    Global.
    local-variable     l:    Local to a function.
    script-variable    s:    Local to a |:source|'ed Vim script.
    function-argument  a:    Function argument (only inside a function).
    vim-variable       v:    Global, predefined by Vim.
