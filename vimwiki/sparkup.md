:html:sparkup:

= Usage of the Sparkup vim plugin for HTML =
----

In insert mode press Ctrl-e after typing a structure of tags

== Examples ==

* .container > .col-xs-12 > h1{Hello title} < < .col-xs-12.col-sm-6

Expands to

    <div class="container">
      <div class="col-xs-11">
        <h1>Hello title</h1>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6"></div>
  
* div#header + div#footer

Expands to
  
    <div id="header"></div>
    <div id="footer"></div>
    
* a[href=index.html][target=_blank]{Home}

Expands to:

  <a target="_blank" href="index.html">Home</a>
  
* ul > li.item-$*3

Expands to
  
    <ul>
        <li class="item-1"></li>
        <li class="item-2"></li>
        <li class="item-3"></li>
    </ul>
