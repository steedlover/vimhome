= VIM commands and shortcuts =
----

== Config ==

* Update the [vimrc](vimrc) config file without reopening vim

== Search ==

* Find a [pattern](pattern) with the *grep* command in the project files
* Slash button search with [yanked](yanked) string
* [Run](Run) a console command from VIM

== Macros ==

* Recording and applying [macros](macros)

== Plugins ==

* [Sparkup](sparkup) plugin for HTML, common rules of usage
* [Fugitive](Fugitive) plugin for GIT

== Programming VIM ==

* [Variables](Variables) types. Defining and removing
