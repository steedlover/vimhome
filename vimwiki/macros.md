:record:macros:normal:norm:

= Recording and applying macros =

In Normal mode press <q> and name of the register, let's say 2 = <q>2
After recording press <q> again to stop recording

To apply you macros, press <@> and name of the register. <@>2 in our case

For applying the macros to multiple string use :normal command

Execute the macro stored in register a on lines 5 through 10

```
:5,10norm! @a
```

Execute the macro stored in register a on lines 5 through the end of the file.

```
:5,$norm! @a
```

Execute the macro stored in register a on all lines.

```
:%norm! @a
```

Execute the macro store in register a on all lines matching pattern.

```
:g/pattern/norm! @a
```

To execute the macro on visually selected lines, press V and the j or k until the
desired region is selected. Then type :norm! @a and observe the that following
input line is shown.

```
:'<,'>norm! @a
```
