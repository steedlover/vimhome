:git:fugitive:

= Fugitive plugin to command your git =
----

* :Gwrite = _git add {current-file}_
* :Gblame = the current file full history in all related commits
* :Gdiff = Compare your current changes with original ones, split the screen horizontally
* :Gsplit HEAD~3:% = split the window horizontally and shows the current file version 3 commits ago
* :Gedit HEAD~3:% = the same as Gsplit but opens the history file in your current widow
