:search:slash:

= Slash button search with yanked (copied to a register) string
----

After pressing / to enter a search string, you can then use Ctrl-R
and then type the letter representing the register that you want to use.

Shift+8 (*) to seek the string  under the cursor

eg.

* First, "Ayw to yank a word into register A
* Then, / ^R A to put the contents of register A into the search string.
  - " for the string from the last register
