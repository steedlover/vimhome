#!/usr/bin/env bash

bat=$(battery Discharging;battery Charging);
percent="\uf8ef"
charging="\uf0e7"

if [ -z $(battery Charging) ]; then
  charging="\uf742"
fi

# First symbol of the script result. i.e [7]8
initial=$(echo "$bat" | head -c 1);

check_battery() {
  case $initial in
    [1])
      printf "\uf579"
      ;;

    [2])
      printf "\uf57a"
      ;;

    [3])
      printf "\uf57b"
      ;;

    [4])
      printf "\uf57c"
      ;;

    [5])
      printf "\uf57d"
      ;;

    [6])
      printf "\uf57e"
      ;;

    [7])
      printf "\uf57f"
      ;;

    [8])
      printf "\uf580"
      ;;

    [9])
      printf "\uf581"
      ;;

    esac;

    [ -n "$bat" ] && printf " $charging $bat$percent"

}

main() {
  check_battery
}

main

