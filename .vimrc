set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
" Gvim turning top bar off
set guioptions-=T
call vundle#begin()

" Set default indents for every buffer
" Below they will be overwritten by the .editorconfig rules
setlocal ts=4
setlocal sw=4

" Vundle
Plugin 'gmarik/Vundle.vim'

" ------------ "
"   Plugins    "
" ------------ "
Plugin 'scrooloose/nerdtree'

" Git plugin
Plugin 'tpope/vim-fugitive'

" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'ycm-core/YouCompleteMe'
Plugin 'Valloric/MatchTagAlways'
Plugin 'scrooloose/nerdcommenter'
Plugin 'git://github.com/nathanaelkane/vim-indent-guides.git'
Plugin 'tpope/vim-surround'
Plugin 'wincent/command-t'
let g:CommandTMaxFiles = 200000

" Exlude files below from saerching list in command-t
set wildignore+=*.png,*.jpg,*.bmp,*.gif,*.jpeg,*.*~,*.*.swp,".".swa
set wildignore+=*/node_modules/*
set wildignore+=*/bower_components/*
set wildignore+=*/build/*
set wildignore+=*/submodules/*

nnoremap <leader>T :CommandT<CR>
" Select objects by similar indent level
Plugin 'michaeljsmith/vim-indent-object'

" Move between vim panes (ctrl+h|j|k|l)
Plugin 'christoomey/vim-tmux-navigator'
" Vimux allows to execute shell commands fromm VIM
Plugin 'benmills/vimux'

" JavaScript
Plugin 'pangloss/vim-javascript'

" Tern for vim
Plugin 'ternjs/tern_for_vim'

" React Native syntax
Plugin 'mxw/vim-jsx'
let g:jsx_ext_required = 0 " Allow JSX in normal JS files

" PHP error checker
"Plugin 'joonty/vim-phpqa'

" TypeScript
Plugin 'Quramy/tsuquyomi'
" TSQuomy requirement
Plugin 'Shougo/vimproc.vim'
" TS Code Highlighting
Plugin 'leafgarland/typescript-vim'
au BufRead,BufNewFile *.ts  setlocal filetype=typescript

" Godot scripting language
Plugin 'calviken/vim-gdscript3'

" Syntastic. Displaying errors system
Plugin 'scrooloose/syntastic'

" Coffee script
Plugin 'kchmck/vim-coffee-script'
autocmd FileType litcoffee runtime ftplugin/coffee.vim

" Less css style
Plugin 'groenewege/vim-less'
Plugin 'ap/vim-css-color'

" Stylus css style
Plugin 'wavded/vim-stylus'

" Project root
Plugin 'dbakker/vim-projectroot'

" Editorconfig
Plugin 'editorconfig/editorconfig-vim'

" Prettier autoformater
Plugin 'prettier/vim-prettier'

nmap <Leader>py <Plug>(Prettier)
let g:prettier#autoformat = 0
let g:prettier#autoformat_require_pragma = 0
let g:prettier#autoformat_config_present = 1
let g:prettier#exec_cmd_async = 0
let g:prettier#quickfix_auto_focus = 0
autocmd BufWritePre *.html,*.hbs,*.pug Prettier

Plugin 'yegappan/grep'
set switchbuf+=newtab
Plugin 'svermeulen/vim-easyclip'
let g:EasyClipAutoFormat=1

" GitHub colorschema
Plugin 'endel/vim-github-colorscheme'

" Dracula colorschema
Plugin 'dracula/vim', { 'name': 'dracula' }

" Lightline, changes style of the statusbar
Plugin 'itchyny/lightline.vim'

" Blade templates highlights
Plugin 'jwalton512/vim-blade'

" PUG files highlighting
Plugin 'digitaltoad/vim-pug'

" Mustache handlebars highlighting
Plugin 'mustache/vim-mustache-handlebars'

Plugin 'tpope/vim-repeat'

" ELM plugin
Plugin 'andys8/vim-elm-syntax'
"Plugin 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}

" Python plugin
Plugin 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }

" Vim Wiki
Plugin 'vimwiki/vimwiki'

let g:vimwiki_list = [{
  \ 'path': '~/vimwiki/',
  \ 'syntax': 'markdown',
  \ 'ext': '.md',
  \ 'auto_diary_index': 1,
  \ 'automatic_nested_syntaxes': 1
  \ }]

let g:vimwiki_folding='list' "enable folding in vimwiki

Plugin 'ryanoasis/vim-devicons'

" All plugins before this line "
call vundle#end()            " required
filetype plugin indent on    " required

" The function below allows to start any 'command'
" after confirmation with the 'question'
function! PerformAction(question, command)
  let answer = confirm(a:question, "&Yes\n&No", 1)
  if answer == 1
    execute a:command
  endif
endfunction

" ------------- "
"    Mapping    "
" ------------- "

" Close error window
nnoremap <Leader>x :lclose<CR>

" Select column of text (C-v by default) but since
" I overwrote this we would need a new combination
nnoremap <S-v>v <C-v>

" Delete current buffer
nnoremap <C-b>d :call PerformAction("Delete buffer \"". expand('%:t') . "\"?", "bdelete")<CR>

" Update vim config
nnoremap <Leader>r :source ~/.vimrc<CR>

" Paste
map <C-v> "+p

" Copy
vmap <C-c> "+yi<Esc>

" Zoom font size Alt+Shift +/-"
" Work only in GUI mode (Gvim)
map <A-+> :LargerFont<CR>
map <A-_> :SmallerFont<CR>
"map <F3> a<C-R>=strftime("%c")<CR><Esc>

" Open NERD tree sidebar "
let g:NERDTreeShowHidden = 1
let g:NERDTreeIgnore = [
  \   '\.pyc$',
  \   '\~$',
  \   '\.sw[op]$'
  \ ] "ignore files in NERDTree
silent! map <leader>tr :NERDTreeToggle<CR>
nnoremap <F5> :CommandTFlush<CR>

" Remap shortcuts for creating marks
"nnoremap <C-m>d :delmarks<CR>
nnoremap <C-m> m

" Move tabs left and right (Ctrl+[ | ])
" Swap the current tab with the left and right ones (Ctrl+z, Shift+[ | ])
nnoremap <C-z>{ :tabm +1<CR>
nnoremap <C-z>} :tabm -1<CR>
nnoremap <C-z>] :tabnext<CR>
nnoremap <C-z>[ :tabprevious<CR>
" DIsable Ctrl-z default action in terminal (send application to background)
map <C-z> <Nop>

setlocal cc=

function! SetColorColumn(w)
  "  set color for textwidth border (if it's been defined)
  if !empty(a:w) && type(a:w) == type(0)
    if a:w > 0
      let &l:textwidth = a:w
      let &colorcolumn=&textwidth
    else
      let &colorcolumn=""
    endif
  endif
endfunction

" ------------------------------------ "
"  Default font type and size          "
"  ----------------------------------- "
"set guifont=DejaVu\ Sans\ Mono\ 10
"set guifont=Anonymice\ Nerd\ Font\ Mono\ 12
set guifont=Hack\ Nerd\ Font\ Mono\ 10

let s:pattern = '^\(.* \)\([1-9][0-9]*\)$'
let s:minfontsize = 6
let s:maxfontsize = 16

function! AdjustFontSize(amount)
  if has("gui_gtk2") && has("gui_running")
    let fontname = substitute(&guifont, s:pattern, '\1', '')
    let cursize = substitute(&guifont, s:pattern, '\2', '')
    let newsize = cursize + a:amount
    if (newsize >= s:minfontsize) && (newsize <= s:maxfontsize)
      let newfont = fontname . newsize
      let &guifont = newfont
    endif
  else
    echoerr "You need to run the GTK2 version of Vim to use this function."
  endif
endfunction

function! LargerFont()
  call AdjustFontSize(1)
endfunction
command! LargerFont call LargerFont()

function! SmallerFont()
  call AdjustFontSize(-1)
endfunction
command! SmallerFont call SmallerFont()
" ------------------------------------ "

" TabStop and ShiftWidth
set ts=2 sw=2 et
" Status line information
set backspace=indent,eol,start

set laststatus=2
" Disable the line below the statusline
" with the lightline plugin properly configured
" this line is annoying
set noshowmode

let g:lightline = {
  \ 'colorscheme': 'solarized',
  \ 'active': {
  \   'left': [ [ 'mode', 'paste' ],
  \             [ 'readonly', 'filename', 'custom' ],
  \             [ 'gitbranch' ] ],
  \   'right': [ ['lineinfo', 'filetype', 'percent' ] ]
  \   },
  \ 'component_function': {
  \   'percent': 'LightlinePercent',
  \   'lineinfo': 'LightlineLineinfo',
  \   'filename': 'LightlineFilename',
  \   'readonly': 'LightlineReadonly',
  \   'gitbranch': 'LightlineFugitive',
  \   'custom': 'LightLineCustom'
  \   },
  \ 'component_expand': {
  \   'filetype': 'LightlineFiletype'
  \ },
  \ 'component_type': {
  \   'filetype': 'warning'
  \ },
  \ 'component_visible_condition': {
  \   'lineinfo': 'CheckLightlineComponentVisibility() > 0'
  \ },
  \ 'separator': { 'left': "\ue0b0", 'right': "\ue0b2" },
  \ 'subseparator': { 'left': "\ue0b1", 'right': "\ue0b3" }
  \ }

" component_visible_condition is used only for subseparators!

function! CheckLightlineComponentVisibility()
  return winwidth(0) > 70 && &filetype !=# "nerdtree" ? 1 : 0
endfunction

" Invisible in small windows and in
" the nerd tree plugin window
function! LightlinePercent()
  return CheckLightlineComponentVisibility() > 0
    \ ? "\uf07d\ " . (100 * line('.') / line('$')) . "%" : ''
endfunction

function! LightlineLineinfo()
  return CheckLightlineComponentVisibility() > 0
    \ ? printf("%3d:%-2d", line('.'), col('.')) : ''
endfunction

function! LightlineFiletype()
  return CheckLightlineComponentVisibility() > 0
    \ ? (&filetype !=# "" ? &filetype : "no ft") : ''
endfunction

function! LightlineFugitive()
  return CheckLightlineComponentVisibility() > 0 && FugitiveStatusline() != ""
    \ ? "\ue5fb\ " . FugitiveStatusline() : ''
endfunction

function! LightlineFilename()
  let filename = expand("%:t") !=# '' ? expand("%:t") : "[No Name]"
  let modified = &modified ? ' +' : ''
  return CheckLightlineComponentVisibility() > 0 ? filename . modified : ''
endfunction
" end invisible in small windows functions"

function! LightlineReadonly()
  return &readonly && &filetype !=# "help" ? "\ue0a2" : ""
endfunction

function! LightLineCustom()
  let exclude = ['vim', 'tmux', 'vimwiki', 'nerdtree', 'help', 'qf']
  if index(exclude, &filetype) >= 0 || CheckLightlineComponentVisibility() == 0
    return ""
  endif

  return exists('b:efi_array') && type(b:efi_array) == type([]) && len(b:efi_array) > 0
        \ ? "[" . join(b:efi_array, ', ') . "]\ \uf64b" : ""
endfunction

function! SetExtraInfoVar(str)
  if !empty(a:str) && type(a:str) == type("")
    " Set the string as another element of the global array of the buffer
    " Only if the substring doesn't exist yet
    if match(b:efi_array, a:str) < 0
      call add(b:efi_array, a:str)
    endif
    return 1
  else
    return 0
  endif
endfunction


" Indent style
set autoindent
set smartindent
set cindent

set exrc
set secure

set number
set relativenumber
set winheight=20
set fileencoding=utf-8

" Select one line and deselect the previuos one
":nnoremap <silent> <Leader>l ml:execute 'match Search /\%'.line('.').'l/'<CR>
" Select multiple lines
nnoremap <silent> <leader>l :call matchadd('Search', '\%'.line('.').'l')<cr>
nnoremap <silent> <leader>lc :call clearmatches()<cr>

" --------------------- "
" --- YouCompleteMe --- "
" --------------------- "
"set complete=.,b,u,],)
set wildmode=longest:list,full
set completeopt=menu,preview

let g:ycm_server_python_interpreter = '/usr/bin/python3'
let g:ycm_complete_in_comments = 1
let g:ycm_semantic_triggers =  {
  \   'c'            : ['->', '.'],
  \   'objc'         : ['->', '.', 're!\[[_a-zA-Z]+\w*\s',
  \                     're!^\s*[^\W\d]\w*\s',
  \                     're!\[.*\]\s'],
  \   'ocaml'        : ['.', '#'],
  \   'cpp,objcpp'   : ['->', '.', '::'],
  \   'perl'         : ['->'],
  \   'ruby'         : ['.', '::'],
  \   'lua'          : ['.', ':'],
  \   'erlang'       : [':'],
  \ }

" ----------- "
"  Searching  "
" ----------- "
" Ignore case when searching
set ignorecase
" Ignore case if search pattern is all lowercase, upcase, etc
set smartcase
" Insert tabs on the start of a line based shiftwidth
set smarttab
" Highlight search terms
set hls
" Show search matches as you type
set incsearch
" Ignore case in search pattern
set is

let g:mta_filetypes = {
      \ 'html':  1,
      \ 'shtml': 1,
      \ 'htm':   1,
      \ 'xhtml': 1,
      \ 'xml':   1,
      \ 'jinja': 1,
      \ 'ejs':   1,
      \ 'tpl':   1
      \}

autocmd BufNewFile,BufRead *.tpl,*ejs  set filetype=html

autocmd filetype make set nocin

" Css color update time
let g:cssColorVimDoNotMessMyUpdatetime = 1

" ----------------- "
"   SparkUp config  "
" ----------------- "
"  Ctr+e in insert mode
"  Ctrl+n - next nested tag
"  Ctrl+p - previous nested tag
let g:sparkupExecuteMapping = '<C-e>'

autocmd CursorMovedI,InsertLeave * if pumvisible() == 0|pclose|endif

" -------------- "
"     Colors     "
" -------------- "
if !has('gui_running')
  set t_Co=256
endif

set background=dark
"colorscheme xoria256 " dark
"colorscheme github " light
colorscheme dracula

"let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 0
let g:indent_guides_start_level = 1
let g:indent_guides_guide_size  = 1
autocmd VimEnter * :IndentGuidesEnable

" colors for a light colortheme
"autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#d7d7d7 ctermbg=255
"autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#ededed ctermbg=253
"
" colors for a dark colortheme
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#212121 ctermbg=235
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#131313 ctermbg=232

" ----------------------------------- "
" Autimatically guessing project root "
" ----------------------------------- "
"nnoremap <expr> <leader>ep ':edit '.projectroot#guess().'/'
let g:rootmarkers = [
  \   '.git',
  \   '.gitignore',
  \   '.prettierrc',
  \   '.prettierignore',
  \   'node_modules',
  \   'bower_components',
  \   'tsconfig.json',
  \   'jsconfig.json',
  \   'tslint.json',
  \   '.jshint',
  \   '.jslint'
  \ ]

function! <SID>AutoProjectRootCD()
  try
    if &ft != 'help'
      ProjectRootCD
    endif
  catch
    " Silently ignore invalid buffers
  endtry
endfunction

" Set Project Root as variable
function! ProjectRootGet()
  return projectroot#get()
endfunction

" Detect local prettier in a project¬
if filereadable(ProjectRootGet() . "/node_modules/.bin/prettier")
  let g:prettier#exec_cmd_path = ProjectRootGet() . "/node_modules/.bin/prettier"
endif

" XML formatter
" Prettier doesn't work with XML files at least for now
" So the best way what I've found is to include xml extension
" to .prettierignore file and remap <Leader>py combination
" to call the function below which uses external program
" for formatting XMLs
" To make it work the tidy external program needs to be installed
" sudo apt-get tidy (ubuntu way to do it)
function! DoFormatXML() range
  " Save the file type
  let l:origft = &ft
  let l:shiftWidth = &sw

  " Clean the file type
  set ft=

  " Add fake initial tag (so we can process multiple top-level elements)
  exe ":let l:beforeFirstLine=" . a:firstline . "-1"
  if l:beforeFirstLine < 0
    let l:beforeFirstLine=0
  endif
  exe a:lastline . "put ='</PrettyXML>'"
  exe l:beforeFirstLine . "put ='<PrettyXML>'"
  exe ":let l:newLastLine=" . a:lastline . "+2"
  if l:newLastLine > line('$')
    let l:newLastLine=line('$')
  endif

  " Remove XML header
  exe ":" . a:firstline . "," . a:lastline . "s/<\?xml\\_.*\?>\\_s*//e"

  " Recalculate last line of the edited code
  let l:newLastLine=search('</PrettyXML>')

  " Execute external formatter
  let l:tidyArgs = "--indent yes --indent-spaces " . l:shiftWidth . " --indent-attributes yes "
  let l:tidyArgs .= "--input-xml yes --output-xml yes --tidy-mark no "
  let l:tidyArgs .= "--quiet yes --show-errors 0 --show-warnings no --clean yes "
  "let l:tidyArgs .= "--preserve-entities yes" " Not to convert HTML entities like & -> &amp;
  exe ":silent " . a:firstline . "," . l:newLastLine . "!tidy " . l:tidyArgs

  " Recalculate first and last lines of the edited code
  let l:newFirstLine=search('<PrettyXML>')
  let l:newLastLine=search('</PrettyXML>')

  " Get inner range
  let l:innerFirstLine=l:newFirstLine+1
  let l:innerLastLine=l:newLastLine-1

  " Remove extra unnecessary indentation
  exe ':silent ' . l:innerFirstLine . ',' . l:innerLastLine 's/^\s\{2,' . l:shiftWidth . '\}//e'

  " Remove fake tag
  exe l:newLastLine . "d"
  exe l:newFirstLine . "d"

  " Put the cursor at the first line of the edited code
  exe ":" . l:newFirstLine

  " Restore the file type
  exe "set ft=" . l:origft
endfunction
command! -range=% FormatXML <line1>,<line2>call DoFormatXML()

autocmd BufWritePre *.xml %FormatXML
autocmd filetype xml nnoremap <Leader>py :%FormatXML<CR>

"disable syntastic on a per buffer basis (some work files blow it up)
function! SyntasticDisableBuffer()
  let b:syntastic_skip_checks = 1
  SyntasticReset
  echo 'Syntastic disabled for this buffer'
endfunction

command! SyntasticDisableBuffer call SyntasticDisableBuffer()
" Global options for syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1


"On open a new one or change the current buffer
function! BufEnterFunc()
  " Try to guess the project root first
  call <SID>AutoProjectRootCD()

  let arr = getbufvar(bufnr("%"), "efi_array")
  if type(arr) != type([])
    let b:efi_array = []
  endif

  if match(&filetype, 'php') >= 0
    call PhpInitOnStart()
  elseif match(&filetype, 'javascript') >= 0
    call JavaScriptInitOnStart()
  elseif match(&filetype, 'handlebars') >= 0
    call HbsInitOnStart()
  elseif match(&filetype, 'xml') >= 0
    call XmlInitOnStart()
  elseif match(&filetype, 'html') >= 0
    call HtmlInitOnStart()
  elseif match(['stylus', 'scss', 'css'], &filetype) >= 0
    call CssInitOnStart()
  elseif match(&filetype, 'python') >= 0
    call PythonInitOnStart()
  elseif match(&filetype, 'typescript') >= 0
    call TypeScriptInitOnStart()
  elseif match(&filetype, 'json') >= 0
    call JsonInitOnStart()
  endif
endfunction


" Exec the function every time on loading/changing a buffer
autocmd BufEnter * call BufEnterFunc()

" ------------ "
"     XML      "
" ------------ "


function! XmlInitOnStart()
  setlocal nocin
  " Disable checker for html
  let g:syntastic_html_checkers     = ['']
  let g:prettier#config#parser      = ''
  let g:ycm_semantic_triggers.xml   = ['<', '"', '</', ' ']
  call SetColorColumn(80)
endfunction


" ------------ "
"     CSS      "
" ------------ "


function! CssInitOnStart()
  setlocal nocin

  let g:ycm_semantic_triggers =  {
    \   'scss,css,styl' : [ 're!^', 're!^\s+', ': ' ],
    \ }

  set omnifunc=csscomplete#CompleteCSS
endfunction

" ------------ "
"  Handlebars  "
" ------------ "


function! HbsInitOnStart()
  setlocal nocin
  " Disable checker for html
  let g:syntastic_html_checkers     = ['']
  let g:prettier#config#parser      = 'html'
  let g:ycm_semantic_triggers['html.handlebars']  = ['{{#', '{{/', '<', '"', '</', ' ']
  call SetColorColumn(160)
endfunction


" ------------ "
"     HTML     "
" ------------ "


function! HtmlInitOnStart()
  setlocal nocin
  " Disable checker for html
  let g:syntastic_html_checkers     = ['']
  let g:prettier#config#parser      = 'html'
  let g:ycm_semantic_triggers.html  = ['<', '"', '</', ' ']
  call SetColorColumn(160)
endfunction


" ------------- "
"  Javascript   "
" ------------- "


function! JavaScriptInitOnStart()
  " Set the checker for javascript depending on what
  " config file was found in the root folder of the project
  if findfile('.eslintrc', '.;') != ''
    let g:syntastic_javascript_checkers    = ['eslint']
  else
    let g:syntastic_javascript_checkers    = ['jshint']
    let g:syntastic_jshint_exec            = "jshint"
    let g:syntastic_javascript_jshint_exec = "jshint"
  endif

  " Check for the local jshint version, usually installed by- npm install jshint --save-dev
  if filereadable(ProjectRootGet() . "/node_modules/jshint/bin/jshint")
    let g:syntastic_jshint_exec = ProjectRootGet() . "/node_modules/jshint/bin/jshint"
    let g:syntastic_javascript_jshint_exec = ProjectRootGet() . "/node_modules/jshint/bin/jshint"
    call SetExtraInfoVar("JS hint") " Update script the extra file info variable
  endif


  let g:ycm_filter_diagnostics.javascript = {
    \   "level": "error"
    \ }
  let g:ycm_semantic_triggers.javascript  = ['.', 're!(?=[a-zA-Z]{3,4})']

  " Prettier parser for javascript
  let g:prettier#config#parser = 'babel'
  " Switch off the errors list of prettier
  " in my case Syntastic does this job
  let g:prettier#quickfix_enabled = 0

  let g:javascript_enable_domhtmlcss = 1

  " Tern JS config
  let g:tern_show_argument_hints = 'on_move'
  " Turn off native mapping
  let g:tern_map_keys = 0

  nnoremap <Leader>jd :YcmCompleter GoTo<CR>
  nnoremap <Leader>jr :YcmCompleter GoToReferences<CR>
endfunction


" ------------ "
"  JSON        "
"  ----------- "

function! JsonInitOnStart()
  "au BufRead,BufNewFile *.json set filetype=json
  let g:syntastic_json_checkers     = ['jsonlint']
endfunction


" ------------ "
"  Typescript  "
"  completion  "
"  and shcuts  "
" ------------ "


function! TypeScriptInitOnStart()
  nmap <buffer> <Leader>tt : <C-u>echo tsuquyomi#hint()<CR>
  nmap <buffer> <Leader>dd : <C-u>TsuquyomiDefinition<CR>

  let g:ycm_semantic_triggers.typescript = ['.']

  let g:tsuquyomi_completion_detail      = 0
  " It's not possible to disable quickfix errors displaying
  " of the tsuquyomi plugin, so it's better off to turn off syntastic
  let g:syntastic_typescript_checkers    = []
  " And prettier just in case
  let g:prettier#quickfix_enabled        = 0
  " Set prettier parser for typescript manually
  let g:prettier#config#parser           = 'babel'

  nnoremap <Leader>jt :YcmCompleter GetType<CR>
  nnoremap <Leader>jd :YcmCompleter GoTo<CR>
  nnoremap <Leader>jr :YcmCompleter GoToReferences<CR>
  call SetColorColumn(120)
endfunction

" ------------ "
"     ELM
" ------------ "


" ------------ "
"     PHP      "
" ------------ "
" PHP syntax checker config

function! PhpInitOnStart()
  " Switch off syntastic check by default
  SyntasticDisableBuffer
  let g:syntastic_php_checkers = ["php", "phpcs", "phpmd"]
  " Bind keys for syntastic toggling off
  nnoremap <silent> <leader>st :SyntasticToggleMode<CR>

  let l:php_codesniffer_severity = "--error-severity=3 --warning-severity=8"

  " Set local project code sniffer handler if exists
  "if filereadable(ProjectRootGet() . "/vendor/bin/phpcs")
    "let g:phpqa_codesniffer_cmd = "" . ProjectRootGet() . "/vendor/bin/phpcs"
  "endif
  

  let g:ycm_semantic_triggers.php = ['->', '::']

  " Try to find a local sniffer's rules file for Joomla
  let l:phpcs_joomla_rules_path = "/vendor/joomla/coding-standards/Joomla/ruleset.xml"
  if filereadable(ProjectRootGet() . l:phpcs_joomla_rules_path)
    call SetExtraInfoVar("Joomla CS rules") " Update script the extra file info variable
    "let g:phpqa_codesniffer_args = join(["--standard=" . ProjectRootGet() . l:phpcs_joomla_rules_path, l:php_codesniffer_severity], " ")
    let g:syntastic_php_phpcs_args = join(["--standard=" . ProjectRootGet() . l:phpcs_joomla_rules_path, l:php_codesniffer_severity], " ")
  else
    "let g:phpqa_codesniffer_args = join(["--standard=PEAR", l:php_codesniffer_severity], " ")
    let g:syntastic_php_phpcs_args = join(["--standard=PEAR", l:php_codesniffer_severity], " ")
  endif

  call SetColorColumn(150)
endfunction

" ------------ "
"    PYTHON    "
" ------------ "
function! PythonInitOnStart()
  let g:pymode_rope_lookup_project   = 0
  let g:syntastic_python_checkers    = ['pyflakes']

  let g:ycm_semantic_triggers.python = ['.']
  call SetColorColumn(80)
endfunction

" ----------- "
"     PUG     "
" ----------- "
function! PugInitOnStart()
  let g:prettier#config#parser     = 'html'
  let g:syntastic_pug_checkers     = ['pug_lint']
  call SetColorColumn(120)
endfunction

" ------------ "
"    Footer    "
" ------------ "
"set winaltkeys=no
set nocompatible

" Allow change buffer without saving
set hidden
" Stops searching at the end of a file
" Super usefull when using recursive macros
set nowrapscan
syntax on

" Highlighting the white spaces
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<
set list

" Color of the matching brackets
hi MatchParen ctermbg=243 ctermfg=232 guifg=white guibg=black
" Color of the EOL spaces in the end of line
hi NonText ctermfg=8 ctermfg=7 guibg=#808080 guifg=#c1c1c1
" Color of the line number where cursor is
set cursorline
hi CursorLine ctermbg=240 ctermfg=250 guibg=#585858 guifg=#bcbcbc
